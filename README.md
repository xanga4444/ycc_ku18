# README #

This document contains a basic setup and quick tutorial to commit the participants project to the repository.

### 1. Basic Setup ###
Setup Git 2.4 or later in your machine.
#### Helpful Links ####

* [Link 1 Mac](https://bitbucket.org/tutorials/markdowndemo)
* [Link 2 Windows](https://bitbucket.org/tutorials/markdowndemo)
* [Link 3 Linux](https://bitbucket.org/tutorials/markdowndemo)


#### Sanity Check ####

code to check current git version 

### 2. Finding Team folder and access rights ###

* Step 1 
* Step 2
* Step 3 
* Step 4

### 3. Seed and Commit ###

* Step 1 
* Step 2
* Step 3 
* Step 4

### 4. Tracking progress ###

* Step 1 
* Step 2
* Step 3 
* Step 4


## Frequently Asked Questions ##

#### Question 1 ####
Answer 1 long short Medium


#### Question 2 ####
Answer 2 long short Medium


#### Question 3 ####
Answer 3 long short Medium


#### Question 4 ####
Answer 4 long short Medium
